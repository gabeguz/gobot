package local

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"gitlab.com/gabeguz/gobot"
)

type Bot struct {
	logger *log.Logger
}

func New() gobot.Bot {
	return &Bot{
		logger: log.New(os.Stderr, "", log.LstdFlags),
	}
}

func (b *Bot) FullName() string {
	return "Bot"
}

func (b *Bot) Name() string {
	return "Bot"
}

func (b *Bot) Send(msg string) {
	fmt.Print(msg)
}

func (b *Bot) Reply(orig gobot.Message, msg string) {
	fmt.Println(msg)
}

func (b *Bot) Connect() error {
	// No connection needed for local bot
	return nil
}

func (b *Bot) Listen() chan gobot.Message {
	msgChan := make(chan gobot.Message)

	go func() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			msgChan <- &LocalMessage{body: scanner.Text()}
		}
		if err := scanner.Err(); err != nil {
			b.Log("Error reading from stdin: " + err.Error())
		}
	}()

	return msgChan
}

func (b *Bot) SetLogger(logger *log.Logger) {
	b.logger = logger
}

func (b *Bot) Log(msg string) {
	b.logger.Println(msg)
}

type LocalMessage struct {
	body string
}

func (lm *LocalMessage) Body() string {
	return lm.body
}

func (lm *LocalMessage) From() string {
	return "Local User"
}

func (lm *LocalMessage) Room() string {
	return "Local"
}
