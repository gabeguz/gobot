package ollama

import (
	"context"
	"log"

	"github.com/ollama/ollama/api"
	"gitlab.com/gabeguz/gobot"
)

type Ollama struct{}

func (p Ollama) Name() string {
	return "Ollama v1.0"
}

func (p Ollama) Execute(msg gobot.Message, bot gobot.Bot) error {

	client, err := api.ClientFromEnvironment()
	if err != nil {
		log.Printf("Error creating ollama client: %s", err)
	}

	messages := []api.Message{
		api.Message{
			Role:    "user",
			Content: msg.Body(),
		},
	}

	ctx := context.Background()
	req := &api.ChatRequest{
		Model:    "llama3",
		Messages: messages,
	}

	respFunc := func(resp api.ChatResponse) error {
		bot.Send(resp.Message.Content)
		return nil
	}

	err = client.Chat(ctx, req, respFunc)
	if err != nil {
		log.Printf("Error with chat: %s", err)
	}

	return nil
}
