// the bot command starts a server
// the bot then connects to a chat service like irc, slack, matrix, etc
// if no service is specified, the bot simply listens for local messages from the chat command
// once connected the bot listens for input.
// the bot recognizes commands in the user input and can then use various plugins to action on those commands

package main

import (
	"flag"
	"fmt"
	"log"
	"strings"

	gb "gitlab.com/gabeguz/gobot"
	"gitlab.com/gabeguz/gobot/local"
	"gitlab.com/gabeguz/gobot/plugins/beer"
	"gitlab.com/gabeguz/gobot/plugins/chatlog"
	"gitlab.com/gabeguz/gobot/plugins/cron"
	"gitlab.com/gabeguz/gobot/plugins/echo"
	"gitlab.com/gabeguz/gobot/plugins/ollama"
	"gitlab.com/gabeguz/gobot/plugins/quote"
	"gitlab.com/gabeguz/gobot/slack"
	"gitlab.com/gabeguz/gobot/xmpp"
)

var host, user, pass, room, name, protocol, logfile string
var crons strslice

func createBot(plugins []gb.Plugin) gb.Gobot {
	var bot gb.Gobot
	if protocol == "xmpp" {
		bot = gb.Gobot{
			Bot:     xmpp.New(host, user, pass, room, name),
			Plugins: plugins,
		}
	} else if protocol == "slack" {
		bot = gb.Gobot{
			Bot:     slack.New(pass, room, name),
			Plugins: plugins,
		}
	} else {
		bot = gb.Gobot{
			Bot:     local.New(),
			Plugins: plugins,
		}
	}

	return bot
}

func executePlugin(p gb.Plugin, m gb.Message, b gb.Bot) {
	err := p.Execute(m, b)
	if err != nil {
		b.Log(p.Name() + " => " + err.Error())
	}
}

func main() {
	// Define + parse command line flags
	flag.StringVar(&host, "host", "", "Hostname:port of the XMPP server")
	flag.StringVar(&user, "user", "", "Username of XMPP server (i.e.: foo@hostname.com")
	flag.StringVar(&pass, "pass", "", "Password for XMPP server")
	flag.StringVar(&room, "room", "", "Room to join (i.e.: #myroom@hostname.com")
	flag.StringVar(&name, "name", "gobot", "Name of the bot")
	flag.StringVar(&protocol, "protocol", "local", "Protocol (local, xmpp, slack)")
	flag.StringVar(&logfile, "logfile", "/tmp/chatlog", "Path to log file")
	flag.Var(&crons, "job", "List of jobs")
	flag.Parse()

	chatlog := chatlog.ChatLog{Filename: logfile}

	plugins := []gb.Plugin{
		echo.Echo{},
		beer.Beer{},
		ollama.Ollama{},
		quote.Quote{},
		chatlog,
	}

	bot := createBot(plugins)
	err := bot.Connect()
	if err != nil {
		log.Fatalln(err)
	}

	for _, crn := range crons {
		parts := strings.Split(crn, "|")
		cron.NewCron(parts[2], crn, bot)
	}

	var msg gb.Message
	var plugin gb.Plugin
	for msg = range bot.Listen() {
		for _, plugin = range bot.Plugins {
			go executePlugin(plugin, msg, bot)
		}
	}
}

type strslice []string

func (s *strslice) String() string {
	return fmt.Sprintf("%s", *s)
}

func (s *strslice) Set(value string) error {
	*s = append(*s, value)
	return nil
}
