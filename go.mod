module gitlab.com/gabeguz/gobot

go 1.22.3

require (
	github.com/matrix-org/gomatrix v0.0.0-20220926102614-ceba4d9f7530
	github.com/mattn/go-xmpp v0.0.1
	github.com/nlopes/slack v0.6.0
	github.com/ollama/ollama v0.1.38
	github.com/otremblay/jkl v0.0.0-20200120155337-493275c780c9
	github.com/robfig/cron v1.2.0
	github.com/stathat/go v1.0.0
	github.com/thatguystone/swan v0.0.0-20190904205542-d1079a5d0c05
)

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/andybalholm/cascadia v1.0.0 // indirect
	github.com/gorilla/websocket v1.2.0 // indirect
	github.com/joho/godotenv v1.2.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/saintfish/chardet v0.0.0-20120816061221-3af4cd4741ca // indirect
	github.com/tdewolff/minify/v2 v2.3.8 // indirect
	github.com/tdewolff/parse/v2 v2.3.5 // indirect
	golang.org/x/crypto v0.23.0 // indirect
	golang.org/x/net v0.25.0 // indirect
	golang.org/x/text v0.15.0 // indirect
)
