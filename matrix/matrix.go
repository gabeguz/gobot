package matrix

import (
	"log"
	"os"

	"github.com/matrix-org/gomatrix"
	"gitlab.com/gabeguz/gobot"
)

type MatrixBot struct {
	UserID      string
	AccessToken string
	Homeserver  string
	RoomID      string
	client      *gomatrix.Client
	logger      *log.Logger
}

func New(userID, accessToken, homeserver, roomID string) gobot.Bot {
	return &MatrixBot{
		UserID:      userID,
		AccessToken: accessToken,
		Homeserver:  homeserver,
		RoomID:      roomID,
		logger:      log.New(os.Stderr, "", log.LstdFlags),
	}
}

func (b *MatrixBot) FullName() string {
	return b.UserID
}

func (b *MatrixBot) Name() string {
	return b.UserID
}

func (b *MatrixBot) Send(msg string) {
	_, err := b.client.SendText(b.RoomID, msg)
	if err != nil {
		b.Log("Error sending message: " + err.Error())
	}
}

func (b *MatrixBot) Reply(orig gobot.Message, msg string) {
	b.Send(msg) // Simplified for demonstration
}

func (b *MatrixBot) Connect() error {
	client, err := gomatrix.NewClient(b.Homeserver, b.UserID, b.AccessToken)
	if err != nil {
		return err
	}
	b.client = client
	return nil
}

func (b *MatrixBot) Listen() chan gobot.Message {
	msgChan := make(chan gobot.Message)
	// Implement message listening logic here
	return msgChan
}

func (b *MatrixBot) SetLogger(logger *log.Logger) {
	b.logger = logger
}

func (b *MatrixBot) Log(msg string) {
	b.logger.Println(msg)
}
