# bot

bot is a simple chatbot written in go.

## Architecture

There are 3 main components:

### Bot

The chatbot. The bot is what the end-user interacts with. The bot is
responsible for listening for text from the user(s) and routing those messages
to various plugins which act on behalf of the bot. 


### Chat Services

Each chat service implements the ChatService interface. It is responsible for
estabilshing a conection to a chat service (local, xmpp, slack, matrix, etc)
and providing the Bot with the basic primitives for interacting with that
service. 


### Plugins/

Plugins recognize wake words, or messages and respond to them. Multiple plugins
could respond to the same input, but their root command should be unique globally.

Plugins are sent every message that goes through the bot weather via a channel
that the bot is listening to, or via private message directly to the bot. Each
plugin must decide if it needs to do anything with the message and if not, just
carry on. If something must be returned to the bot, the plugin just returns it
in plain text?

Plugins that exist currently (various states of working)

beer - lets us know if it's time for a beer at work
chatlog - logs all messages to a text file on the server
echo - just echos back what you tell it
quote - returns quotes
ollama - send messages to an ollama server


## Install

Make sure you have Go installed, then: 

```
go install gitlab.com/gabeguz/bot@latest
```

## Running a bot

bot supports both slack and XMPP (jabber).

To connect to slack you'll need an API token, directions for getting one can be
found at https://api.slack.com/bot-users aside from that, pick a name for your
bot and enter the room you would like your bot to join.

```
$ bot -protocol=slack -user=herman -pass=<slack api token> -room=bottest -name=herman
```

For xmpp you need a few extra bits of information.  The hostname with port, the
abber user, the users password, and the full address to the MUC you'd like the
bot to join:

```
$ bot -protocol=xmpp -host="jabber.my.domain:5222" -user="gabe@jabber.my.domain" -pass="hunter2" -room="#bottest@conference.jabber.my.domain" -name="herman"
```

## Creating A Plugin

Creating a plugin is pretty easy, you just need to import the bot library:

```
import (
	"gitlab.com/gabeguz/bot"
)
```

And then implement 2 exported methods so that you conform to the bot.Plugin interface:

```
type MyPlugin struct {}

func (p MyPlugin) Name() string {
	return "MyPlugin v1.0"
}

func (p MyPlugin) Execute(msg bot.Message, bot bot.Bot) error {
	if msg.From() != bot.FullName() {
		bot.Send("Why, hello!")
	}
	return nil
}
```

One last step, edit bot/cmd/main.go and add a call to your new plugin
to the []gb.Plugin slice:

```
plugins := []gb.Plugin{
	myplugin.MyPlugin{},
}
```
and import the code in the `import` statement at the top of the file.

Finally, reinstall bot `$ cd $GOPATH/src/gitlab/gabeguz/bot/cmd/bot && go
install`
